-- SUMMARY --

AJAX based galleries module is (partially) based on mockup seen on web - http://www.raincitystudios.com/blogs-and-pods/hubert/outline-ui-design-ajax-image-gallery

Summarizing pros/cons for image/imagefield I decided to go with image nodes. Also didn't use views/taxonomy, because node has no weight field, so I had to make my own structure.

This module uses a lot of jquery (and contributed jquery plugins) so update it properly. UI simplicity is kept in mind while making it.

Multiple file upload done using SWFUpload from http://www.swfupload.org


-- REQUIREMENTS --

jquery_update
image


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

No configuration for module yet

-- CONTACT --

Current maintainers:
* Janis Bebritis (Jancis) - jancis@iists.it

This project has been sponsored by:
* PROG (www.prog.lv)
  Specialized in consulting and planning of Drupal powered sites, PROG offers 
  installation, development, theming, customization, and hosting
  to get you started. Visit http://www.prog.lv for more information.

-- SPECIAL THANKS --

Big thanks for helpfull hand and inspiration goes to Mi�elis Za�ais (mike-green). Also thanks for testing and stuff, you're in my README file now ;)
